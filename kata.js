function list(names) {  
    let nameString = ""
    for (let i = 0; i < names.length; i++) {
        nameString += `${names[i].name}`
        if (i == names.length - 2) { 
            nameString += ` & `
        } else if (i < names.length - 2){
            nameString += `, `
        }
    }
    return(nameString)
}


console.log(list([{name: 'Bart'},{name: 'Lisa'},{name: 'Maggie'},{name: 'Homer'},{name: 'Marge'}]))
console.log(list([{name: 'Bart'},{name: 'Lisa'},{name: 'Maggie'}]))
console.log(list([{name: 'Bart'},{name: 'Lisa'}]))
console.log(list([{name: 'Bart'}]))
console.log(list([]))